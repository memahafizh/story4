from django.shortcuts import render

# Create your views here.
def index(request):
    context = {"check" :0}
    return render(request, 'index.html', context)

def exp(request):
    context = {"check" :1}
    return render(request, 'exp.html', context)

def gallery(request):
    context = {"check" :2}
    return render(request, 'gallery.html', context)

def contact(request):
    context = {"check" :3}
    return render(request, 'contact.html', context)